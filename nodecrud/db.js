const mongoose = require ('mongoose')

let uri = 'mongodb://localhost:27017/CrudDB'

mongoose.connect(uri, {
    useUnifiedTopology: true,
    useNewUrlParser: true},
    (err)=>{
        if (err){
            console.log('connection error')
        }
        else{
            console.log('connected to the db')
        }
    });

module.exports = mongoose;