const express = require("express");
const bodyParser = require("body-parser");

const { mongoose } = require("./db");
const employeeController = require('./controllers/employeeController')

const app = express();
app.use(bodyParser.json());

app.listen(3002, () => console.log("server started at port: 3002"));

app.use('/employees', employeeController)