var mongoose = require("mongoose");
const chai = require("chai");
const expect = chai.expect;
chaiHttp = require('chai-http');
chai.use(chaiHttp);

var uri = "mongodb://localhost:3001/";
mongoose.connect(uri, { useUnifiedTopology: true, useNewUrlParser: true });

const Book = require("../book/book.model");

describe("Book Model", () => {
  after(async () => {
    await Book.remove({});
    await mongoose.connection.close();
  });

  it("has a module", () => {
    expect(Book).to.not.be.undefined;
  });

  describe('bookStore', () => {
    it('#get', async (done) => {
      const book = new Book({ title: "title", author: "author", rating: 5 });
      await book.save();

      const foundBook = await Book.findOne({ title: "title" });
      const expected = "title";
      const actual = foundBook.title;
      expect(actual).equal(expected);
        });
    })
  })