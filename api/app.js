"use strict";
var express = require("express");
var bodyParser = require("body-parser");
var mongoose = require("mongoose");
var request = require('request');
const BookService = require("./book");

var app = express();

mongoose.Promise = global.Promise;

var uri = "mongodb://localhost:27017/bookstore";
mongoose.connect(uri, {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    function(err){
        if (err){
            return err
        }console.log('connected to the db')
    }
});

app.use(
    bodyParser.urlencoded({
        extended: false,
    })
);
app.use(bodyParser.json());

// app.get("/listBooks", async function (req, res, next) {
//     try {
//         const books = await BookService.listBooks();
//         res.json(books);
//     } catch (e) {
//         next(e);
//     }
// });

app.post("/addBook", async function (req, res, next) {
  const title = req.body.title;
  const author = req.body.author;
  const rating = req.body.rating;

  console.log("###", req.body.title)
  try {
    const books = await BookService.addBook(title, author, rating);
    console.log(res)
    res.json(books);
  } catch (e) {
    next(e);
  }
});
app.get("/addBook", async function (req, res, next) {
    try {
        const books = await BookService.listBooks();
        res.json(books);
    } catch (e) {
        next(e);
    }
})
app.listen(3001, function () {
    console.log("server starting on localhost:3001");
});



module.exports = app;